/* eslint-disable no-undef */
/* <reference types="Cypress" /> */

describe("Testing full user journey", () => {
  console.log("HEREEEE", Cypress.env("GATEWAY_URL"));
  it("Visits My Family Tree App", () => {
    cy.visit(Cypress.env("GATEWAY_URL"));
  });
  it("Clicks the login link and the form appears", () => {
    cy.get("[data-testid=login-link]").should("be.visible").click();
    cy.get(".login-form").should("be.visible");
  });
  it("An error is returned if incorrect email and password is entered", () => {
    cy.get(".btn-lg").should("be.visible").click();
    cy.get(".alert").should("be.visible");
  });
  it("The user can successfully log in", () => {
    cy.get("#email").should("be.visible").type("joedoe@fake.com");
    cy.get("#password").should("be.visible").type("pass123");
    cy.get(".btn-lg").click();
    cy.get(".App-body > :nth-child(1) > :nth-child(1)").should("be.visible");
  });
  it("The user can navigate through the app via the nav/toolbar", () => {
    cy.get("[data-testid=about-link]").should("be.visible").click();
    cy.get("h2").contains("About us page");
    cy.get("[data-testid=allAncestors-link]").should("be.visible").click();
  });
  it("The user can filter the Anscestory results via the dropdown", () => {
    cy.get("select").select("descending").select("ascending");
  });
  it("A user can add a new Ancestor via the form on My Family Tree", () => {
    cy.get("[data-testid=myFamilyTree-link]").should("be.visible").click();
    cy.get("#ancestor-name").should("be.visible").type("Cypress Test");
    cy.get("#year-of-birth").should("be.visible").type("2000");
    cy.get("#children").should("be.visible");
    cy.get("#job").should("be.visible").type("Automation Tester");
    cy.get("#married").should("be.visible");
    cy.get("#deceased").should("be.visible");
    cy.get(".btn-lg").should("be.visible").click();
  });
  it("The new Ancestor is located on the All Ancestors page", () => {
    cy.get("[data-testid=allAncestors-link]").click();
    cy.contains("Cypress Test").should("be.visible");
  });
  it("The user can log out by clicking the log out link", () => {
    cy.get("[data-testid=logout-link]").should("be.visible").click();
    cy.contains("YOU ARE LOGGED OUT");
  });
});
