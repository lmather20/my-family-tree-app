import firebase from "firebase";

const config = {
  apikey: "AIzaSyAQBDxXSO_vdcYrM7JieZ6a0SVc56gm2z4",
  databaseURL: "https://my-family-tree-198e5.firebaseio.com",
  projectId: "my-family-tree-198e5",
};

firebase.initializeApp(config);

export const db = firebase.firestore();
