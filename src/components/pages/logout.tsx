import React from "react";

import "../compStyle.css";

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export default function Logout() {
  return (
    <div>
      <h2> YOU ARE LOGGED OUT</h2>
      <div className="content-body">
        <></>
      </div>
    </div>
  );
}
