/* eslint-disable @typescript-eslint/ban-ts-ignore */
/* eslint-disable @typescript-eslint/explicit-function-return-type */
/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useState, useEffect } from "react";
import { db } from "../services/firebase";

import "../components/compStyle.css";

import { useLocation } from "react-router-dom";
import { UserDetails } from "../types";
import { Form, FormGroup, Label, Input, Button } from "reactstrap";

function useRegisterForm() {
  const [registerUser, setRegisterUser] = useState<UserDetails[]>([]);

  useEffect(() => {
    const unsubscribe = db.collection("users").onSnapshot((snapshot) => {
      const newUser: UserDetails[] = snapshot.docs.map((doc) => {
        const { name, email, password, gender, yearOfBirth } = doc.data();
        return {
          name,
          email,
          password,
          gender,
          yearOfBirth,
        };
      });
      setRegisterUser(newUser);
    });
    return () => unsubscribe();
  }, []);
  return registerUser;
}

export default function RegisterForm(props: { userDetails?: UserDetails }) {
  const location = useLocation();

  const [name, setName] = useState("");
  const [yearOfBirth, setYearOfBirth] = useState(0);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [gender, setGender] = useState("");

  function onSubmit(e: { preventDefault: () => void }) {
    e.preventDefault();

    db.collection("users")
      .add({
        name,
        email,
        password,
        gender,
        yearOfBirth,
      })
      .then(() => {
        setName("");
        setEmail("");
        setPassword("");
        setGender("");
        setYearOfBirth(0);
      });
  }

  return (
    <div>
      <h2> Register in the form below:</h2>
      <div className="content-body">
        <Form onSubmit={onSubmit} className="register-new-user-form">
          <FormGroup>
            <Label htmlFor="user-name">Full Name</Label>
            <Input
              id="user-name"
              label="user-name"
              type="text"
              placeholder="user-name"
              value={name}
              onChange={(e) => setName(e.currentTarget.value)}
            />
          </FormGroup>
          <FormGroup>
            <Label htmlFor="email">Email</Label>
            <Input
              id="email"
              label="email"
              type="email"
              placeholder="email"
              value={email}
              onChange={(e) => setEmail(e.currentTarget.value)}
            />
          </FormGroup>
          <FormGroup>
            <Label htmlFor="password">New Password</Label>
            <Input
              id="password"
              label="password"
              type="text"
              placeholder="password"
              value={password}
              onChange={(e) => setPassword(e.currentTarget.value)}
            />
          </FormGroup>
          <FormGroup>
            <Label htmlFor="year-of-birth">Year of Birth</Label>
            <Input
              id="year-of-birth"
              label="year-of-birth"
              type="number"
              placeholder="year-of-birth"
              value={yearOfBirth}
              onChange={(e) => setYearOfBirth(e.currentTarget.valueAsNumber)}
            />
          </FormGroup>
          <FormGroup>
            <Label htmlFor="gender">Gender (what you identify as)</Label>
            <Input
              id="gender"
              label="gender"
              type="text"
              Your
              placeholder="gender"
              value={gender}
              onChange={(e) => setGender(e.currentTarget.value)}
            />
          </FormGroup>
          <Button
            onClick={() => alert("Your Account Has Been Registered!!!")}
            className="btn-lg btn-dark btn-block"
          >
            Create your new account
          </Button>
        </Form>
      </div>
    </div>
  );
}
