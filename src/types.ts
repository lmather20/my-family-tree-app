export interface UserDetails {
  [propname: string]: any;
  name: string;
  email: string;
  gender: string;
  password: string;
  yearOfBirth: number;
}

export interface AncestorDetails {
  name: string;
  yearOfBirth: number;
  dead: boolean;
  job: string;
  married: boolean;
  children: number;
}

export interface AncestorTest {
  [propname: string]: any;
  name: string;
  yearOfBirth: number;
  deceased: boolean;
  job: string;
  married: boolean;
  children: number;
}
